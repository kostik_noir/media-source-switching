# Measuring the speed of change MediaSource for HTMLVideoElement

## How it works

1. It loads two video files from `/video` directory using AJAX requests. 
Both video files are video files with size 720x404 and duration 1 minute.
2. Then it create instance of `MediaSource`, append it to HTMLVideoElement, 
add `SourceBuffer`, then fills it with data of one of the video files, and starts playback
3. Then it waits random delay and repeats the step 2 with data of other video file
4. and so on infinitely

During step 2, it measures the time that was required in order to perform each operation 
and render these values in the table. 

## Live

[http://switch-ms-measurement.bitballoon.com/](http://switch-ms-measurement.bitballoon.com/)

This can be slow during startup. Just wait when everything is loaded.

## Results

OS: Kubuntu 15.10 x64  
CPU: 4 x Intel Core i5-3230M @ 2.60 GHz  
RAM: 5.7 GiB  

+ Firefox 43.0.4: 57 ms - 100 ms
+ Chrome 47.0.2526.106 (64-bit): 40 ms - 60 ms

OS: Windows 8.1 (VirtualBox)  
CPU: 1 CPU @ 2.6 GHz  
RAM: 1 GiB  

+ Firefox 43.0.4: 72 ms - 195 ms
+ Chrome 47.0.2526.106 m: 62 ms - 118 ms
+ Opera 34.0.2036.25: 69 ms - 151 ms
+ IE 11: 107 ms - 245 ms
