rm -rf ./tmp
rm -rf ./video

mkdir ./tmp
mkdir ./video

# ------------
# bento4
# ------------
wget http://zebulon.bok.net/Bento4/binaries/Bento4-SDK-1-4-3-607.x86_64-unknown-linux.zip -O ./tmp/bento4.zip
unzip ./tmp/bento4.zip -d ./tmp/
mv "./tmp/$(ls ./tmp/ | grep Bento)" ./tmp/bento4
rm ./tmp/bento4.zip

# ------------
# trailer 1
# ------------
wget http://avideos.5min.com/389/5193389/519338808_4.mp4 -O ./tmp/ice-age.mp4

ffmpeg -y -i ./tmp/ice-age.mp4 \
  -ss 00:00:15 -to 00:01:15 \
  -vf scale=720:404 \
  -c:v libx264 -c:a copy \
  ./tmp/ice-age.tmp.mp4

./tmp/bento4/bin/mp4fragment ./tmp/ice-age.tmp.mp4 ./video/ice-age.mp4

rm ./tmp/ice-age.tmp.mp4

# ------------
# trailer 2
# ------------
wget http://avideos.5min.com/48/5189048/518904717_4.mp4 -O ./tmp/panda.mp4

ffmpeg -y -i ./tmp/panda.mp4 \
  -ss 00:00:35 -to 00:01:35 \
  -vf scale=720:404 \
  -c:v libx264 -c:a copy \
  ./tmp/panda.tmp.mp4
  
./tmp/bento4/bin/mp4fragment ./tmp/panda.tmp.mp4 ./video/panda.mp4
  
rm ./tmp/panda.tmp.mp4
