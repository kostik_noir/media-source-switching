import {appendMediaSource, insertVideoData, waitVideoStartsPlay, randomDelay} from './utils.js';

const HIDDEN_VIDEO_CLASS_NAME = 'video-hidden';
const MIME_TYPE = 'video/mp4; codecs="avc1.64001F, mp4a.40.2"';

const STATS_TPL = '' + 
  '<td>{{INDEX}}</td>' +
  '<td>{{APPEND_MEDIA_SOURCE}}</td>' +
  '<td>{{INSERT_VIDEO_DATA}}</td>' +
  '<td>{{WAIT_VIDEO_STARTS_PLAY}}</td>' +
  '<td>{{STEP_DURATION}}</td>';

export function start(videoData) {
  let videoHolder = document.querySelector('.video-holder');
  let video = document.createElement('video');
  video.classList.add('video');
  videoHolder.appendChild(video);
  video.volume = 0.5;
  
  let statsHolder = document.querySelector('.stats-table');
  let activeMediaSource = null;

  let videoIndex = 0;
  let stepIndex = 0;

  step();

  function step() {
    stepIndex++;

    let stepStart = Date.now();
    let stats = {
      index: stepIndex
    };

    hideVideo(video);
    video.src = '';

    appendMediaSource(video)
      .then(function(result) {
        activeMediaSource = result.mediaSource;
        stats.appendMediaSource = result.stats.duration;
        return insertVideoData(activeMediaSource, videoData[videoIndex], MIME_TYPE);
      })
      .then(function(result) {
        stats.insertVideoData = result.stats.duration;
        video.play();
        return waitVideoStartsPlay(video);
      })
      .then(function(result) {
        stats.waitVideoStartsPlay = result.stats.duration;
        stats.step = Date.now() - stepStart;
        showVideo(video);
        renderStats(statsHolder, stats);
        return randomDelay(activeMediaSource.duration / 5 * 1000);
      })
      .then(function() {
        videoIndex++;
        if (videoIndex >= videoData.length) {
          videoIndex = 0;
        }
        step();
      })
      .catch(function(err) {
        console.error(err);
      })
  }
}

function hideVideo(video) {
  video.classList.add(HIDDEN_VIDEO_CLASS_NAME);
}

function showVideo(video) {
  video.classList.remove(HIDDEN_VIDEO_CLASS_NAME);
}

function renderStats(holder, stats) {
  let tpl = STATS_TPL
    .replace('{{INDEX}}', stats.index)
    .replace('{{APPEND_MEDIA_SOURCE}}', stats.appendMediaSource)
    .replace('{{INSERT_VIDEO_DATA}}', stats.insertVideoData)
    .replace('{{WAIT_VIDEO_STARTS_PLAY}}', stats.waitVideoStartsPlay)
    .replace('{{STEP_DURATION}}', stats.step);
  let el = document.createElement('tr');
  el.innerHTML = tpl;
  holder.appendChild(el);
}
