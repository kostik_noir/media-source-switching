import Deferred from './deferred.js';

export function appendMediaSource(video) {
  let deferred = new Deferred();

  let start = Date.now();

  let mediaSource = new window.MediaSource();
  mediaSource.addEventListener('sourceopen', onSourceOpen, false);

  let sourceUrl = window.URL.createObjectURL(mediaSource);
  video.src = sourceUrl;

  function onSourceOpen() {
    window.URL.revokeObjectURL(sourceUrl);
    mediaSource.removeEventListener('sourceopen', onSourceOpen, false);
    deferred.resolve({
      mediaSource: mediaSource,
      stats: {
        duration: Date.now() - start
      }
    });
  }

  return deferred.promise;
}

export function insertVideoData(mediaSource, videoData, mimeType) {
  let deferred = new Deferred();

  let start = Date.now();

  let buffer = mediaSource.addSourceBuffer(mimeType);
  buffer.addEventListener('updateend', onUpdateEnd, false);
  buffer.appendBuffer(videoData);

  function onUpdateEnd() {
    buffer.removeEventListener('updateend', onUpdateEnd, false);
    deferred.resolve({
      stats: {
        duration: Date.now() - start
      }
    });
  }

  return deferred.promise;
}

export function randomDelay(max) {
  let deferred = new Deferred();

  let delay = Math.floor(Math.random() * max);

  setTimeout(function() {
    deferred.resolve();
  }, delay);

  return deferred.promise;
}

export function waitVideoStartsPlay(video) {
  let deferred = new Deferred();

  let start = Date.now();
  
  video.addEventListener('playing', onPlaying, false);
  
  function onPlaying() {
    video.removeEventListener('playing', onPlaying, false);
    deferred.resolve({
      stats: {
        duration: Date.now() - start
      }
    });
  }
  
  return deferred.promise;
}
