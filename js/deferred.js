export default class Deferred {
  constructor() {
    this._resolveFn = null;
    this._rejectFn = null;

    this.promise = new Promise((resolveFn, rejectFn) => {
      this._resolveFn = resolveFn;
      this._rejectFn = rejectFn;
    });
  }

  resolve() {
    this._resolveFn.apply(this, arguments);
  }

  reject() {
    this._rejectFn.apply(this, arguments);
  }
}
