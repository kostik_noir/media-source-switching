import Deferred from './deferred.js';

export function loadVideoFile(url) {
  let deferred = new Deferred();

  let xhr = new XMLHttpRequest();
  xhr.open('GET', url, true);
  xhr.responseType = 'arraybuffer';
  xhr.addEventListener('readystatechange', onReadyStateChanged, false);
  xhr.send(null);

  function onReadyStateChanged() {
    if (xhr.readyState !== 4) {
      return;
    }

    xhr.removeEventListener('readystatechange', onReadyStateChanged, false);
    
    if (xhr.status < 200 || xhr.status >= 300) {
      let err = {
        status: xhr.status,
        statusText: xhr.statusText
      };
      deferred.reject(err);
      return;
    }

    deferred.resolve(new Uint8Array(xhr.response));
  }

  return deferred.promise;
}

export function loadVideoFiles(urls) {
  let promises = urls.map(function(url) {
    return loadVideoFile(url);
  });
  return Promise.all(promises);
}
