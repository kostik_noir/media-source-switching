import {loadVideoFiles} from './loader.js';
import {start} from './tester.js';

const VIDEO_URLS = [
  './video/panda.mp4',
  './video/ice-age.mp4'
];

loadVideoFiles(VIDEO_URLS)
  .then(function(data) {
    start(data);
  })
  .catch(function(err) {
    console.error(err);
  });
